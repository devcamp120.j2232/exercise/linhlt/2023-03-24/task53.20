import s10.Rectangle;
public class Task5320 {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.0f,3.0f);
        System.out.println(rectangle1);
        System.out.println(rectangle2);
        //lấy diện tích
        System.out.println("Dien tich hinh 1: " + rectangle1.getArea());
        System.out.println("Dien tich hinh 2: " + rectangle2.getArea());
        //lấy chu vi
        System.out.println("Chu vi hinh 1: " + rectangle1.getPerimeter());
        System.out.println("Chu vi hinh 2: " + rectangle2.getPerimeter());
    }
}
